<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buat Account Baru!</title>
</head>
<body>
     <h1>Buat Account Baru!</h1>
     <h3>Sign Up Form</h3>
     <form action="/welcome" method="POST">
        @csrf
         <label>First Name :</label> <br><br>
         <input type="text" name="FirstName"><br><br>
         <label>Last Name :</label><br> <br>
         <input type="text" name="LastName"> <br> <br>
         <label>Gender</label> <br> <br>
         <input type="radio" name="WN" value="1"> Male <br>
         <input type="radio" name="WN" value="2"> Female <br>
         <input type="radio" name="WN" value="3"> Other <br><br>
         <label>Nationality</label> <br> <br>
         <select name="Nationality" id=""> <br>
             <option value="1">Indonesia</option>
             <option value="2">Cina</option>
             <option value="3">Amerika</option>
         </select> <br> <br>

         <Label>Language Spoken</Label><br><br>
         <input type="checkbox" name="language" value="1"> Bahasa Indonesia <br>
         <input type="checkbox" name="language" value="2"> English <br>
         <input type="checkbox" name="language" value="3"> Other <br> <br>

         <label>Bio :</label><br> <br>
         <textarea name="Bio" cols="30" rows="10"></textarea> <br> <br>
         <input type="submit" value="Sign Up">
        </form>
</body>    
</html>