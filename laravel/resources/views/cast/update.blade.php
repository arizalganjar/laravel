@extends('layout.master')

@section('title')
Halaman Update Pemain Film Baru
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Cast</label>
      <input name="nama" value="{{old('nama', $cast->nama)}}" type="text" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur Cast</label>
        <input name="umur" value="{{old('umur', $cast->umur)}}" type="number" class="form-control">
    </div>
    @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio Cast</label>
        <textarea name="bio" cols="30" rows="10" class="form-control">{{old('bio', $cast->bio)}}</textarea>
      </div>
      @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection 