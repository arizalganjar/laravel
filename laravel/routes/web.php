<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::POST('/welcome', 'AuthController@welcome');

//table
Route::get('/table', function(){
    return view('halaman.table');
});

//data-table
Route::get('/data-table', function(){
    return view('halaman.data-table');
});


//CRUD CAST

//Create Data Cast

//masuk ke form cast
Route::get('/cast/create', 'CastController@create');
//untuk kirim inputan ke table cast
Route::post('/cast', 'CastController@store');

//Read Data Cast

//tampil semua data cast
Route::get('/cast', 'CastController@index');
//detail cast berdasarkan id
Route::get('/cast/{cast_id}', 'CastController@show');

//Update Data Cast

//masuk ke form cast berdasarkan id
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//untuk update data inputan berdasarkan id
Route::put('/cast/{cast_id}', 'CastController@update');

//Delete Data Cast

//delete data berdasarkan id
Route::delete('/cast/{cast_id}', 'CastController@destroy');


